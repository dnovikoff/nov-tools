<?xml version="1.0"?>

<xsl:stylesheet version="2.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:p="http://www.w3schools.com"
exclude-result-prefixes="#all"
>

<xsl:output method="xml" encoding="utf-8" indent="yes"/>

<xsl:param name="outputDir" />

<xsl:template match="/">
	<xsl:if test="not($outputDir)">
		<xsl:message terminate="yes">
			Error: You should provide outputDir param
		</xsl:message>
	</xsl:if>

	<xsl:variable name="fileName" select="concat($outputDir, '/test.document')"/>
	<xsl:result-document href="{$fileName}" method="text" >
		Hello
	</xsl:result-document>
</xsl:template>

</xsl:stylesheet> 