<?xml version="1.0"?>

<xsl:stylesheet version="2.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:p="http://www.w3schools.com"
exclude-result-prefixes="#all"
>

<xsl:output method="xml" encoding="utf-8" indent="yes"/>

<xsl:template match="/p:test">
	<xsl:copy-of select="."/>
</xsl:template>

</xsl:stylesheet> 