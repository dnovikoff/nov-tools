#!/bin/bash

set -e

prev_arg=""
cur_arg=""
object_file=""
source_file=""
dep_file=""
is_compilation=true
for var in "$@"
do
	prev_arg=$cur_arg
	cur_arg=$var

	if [ "-DNOV_FAIL_COMPILATION" == "$var" ]
	then
		is_compilation=false
	elif [ "-o" == "$prev_arg" ]
	then
		object_file=$var
	elif [ "-c" == "$prev_arg" ]
	then
		source_file=$var
	elif [ "-MF" == "$prev_arg" ]
	then
		dep_file="$var"
	fi
	prev_arg=$var
done

if [ -z $object_file ]
then
	echo "Object file not found in args: $@"
	false
fi

if [ -z $source_file ]
then
	echo "Source file not found in args: $@"
	false
fi

compile_command="$*"

echo "$compile_command" > $object_file.cmd

if $is_compilation
then
	$compile_command
	#Exit if not failed
	if [ $? == 0 ]
	then
		exit 0
	fi
fi

set +e
$compile_command 2> $object_file.err
r=$?
set -e

if [ $r == 0 ]
then
	rm -rf $object_file
	echo "Error: Expected file to fail compilation. "
	false
fi

if [ $dep_file ]
then
	echo "$object_file: $source_file" > $dep_file
fi

cp $object_file.err $object_file
