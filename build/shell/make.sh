#!/bin/bash

set -e

make_file="Makefile"
make_cmd="make"
cmake_args=""

use_ninja() {
	make_file="build.ninja"
	make_cmd="ninja -d explain"
	cmake_args="-G Ninja"
}

if [ ! -f CMakeLists.txt ]; then
	echo "Folder must conain CMakeLists.txt. Exiting"
	false
fi

build_variant() {
	VARIANT="$1"
	FORCE_CMAKE="$2"
	TARGET="${*:3}"

	VARIANT_DIR=".build/$VARIANT"

	if [ ! -d $VARIANT_DIR ]; then
		echo "'$VARIANT_DIR' dir does not exists. Creating..."
		mkdir -p $VARIANT_DIR
	fi

	if [ ! -f $make_file ]; then
		FORCE_CMAKE=true
	fi

	cd $VARIANT_DIR
	if [ "$FORCE_CMAKE" = true ]; then
		echo "Generating build ..."
		cmake $cmake_args -D CMAKE_BUILD_TYPE=$VARIANT ../..
	fi

	echo "Building ..."

	$make_cmd $TARGET
	
	echo "Done"
	cd ../..
}

VARIANT="release"
ARGS=""
PACK=false
PACKINSTALL=false
CLEAN=false
FORCE_CMAKE=false
DO_BUILD=true

for ARG in $*
do
	if [[ "$ARG" = --* ]]; then
		if [ "$ARG" == "--ninja" ]; then
			use_ninja
		elif [ "$ARG" == "--release" ]; then
			VARIANT="release"
		elif [ "$ARG" == "--debug" ]; then
			VARIANT="debug"
		elif [ "$ARG" == "--pack" ]; then
			ARGS="$ARGS package"
			PACK=true
			DO_BUILD=false
		elif [ "$ARG" == "--install" ]; then
			PACKINSTALL=true
			DO_BUILD=false
			echo "Checking for root password to be able to install"
			sudo true
		elif [ "$ARG" == "--clean" ]; then
			DO_BUILD=false
			CLEAN=true
		elif [ "$ARG" == "--cmake" ]; then
			FORCE_CMAKE=true
		else
			echo "Unknown option $ARG"
			false
		fi
	else
		ARGS="$ARGS $ARG"
	fi
done

echo "Variant is $VARIANT"

VARIANT_DIR=".build/$VARIANT"

if [ "$CLEAN" = true ]; then
	debuild clean
	rm -rf $VARIANT_DIR
	rm -rf debian/*.log
	rm -rf debian/*.debhelper
	rm -rf debian/*.subvatarts
	rm -rf debian/stamp-*
	rm -rf debian/tmp
	rm -rf obj-*
	
	rm -rf ../*.deb
	rm -rf ../*.changes
	rm -rf ../*.build
	find .. -name *.dsc | sed  's/\.dsc$/.tar.gz/g' | xargs rm -rf
	rm -rf ../*.dsc
	false
fi

if [ "$PACK" = true ]; then
	debuild -uc -us
fi

if [ "$DO_BUILD" = true ]; then
	build_variant $VARIANT $FORCE_CMAKE $ARGS
fi

if [ "$PACKINSTALL" = true ]; then
	echo "Install deb"
	files=$(cat debian/files | awk '{print $1}' | xargs echo)
	echo "Installing files $files"
	(cd .. && sudo dpkg -i $files)
fi
