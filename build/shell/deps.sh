#!/bin/bash

set -e
#set -x

if [ ! -f "deps.txt" ]; then
	echo "No deps.txt found"
	exit 1
fi

is_update=false
is_sync=false

for var in "$@"
do
	if [ "update" == "$var" ]
	then
		is_update=true
	elif [ "sync" == "$var" ]
	then
		is_sync=true
	else
		echo "Allowed args are sync/update"
		exit 1
	fi
	prev_arg=$var
done

if $is_update; then
	mkdir -p .checkout
	cat "deps.txt" | while read line
	do
		a=( $line )
		url=${a[0]}
		dir=${a[1]}
		sd=${a[2]}
		cloneto=".checkout/$dir"
		subdir="$cloneto/$sd"
		if [ ! -d $cloneto ]; then
			git clone $url $cloneto
		fi
		CUR=$(pwd)
		cd $cloneto
		git pull origin master
		git clean -ffdx
		cd $CUR
	done
fi

if $is_sync; then
	if [ ! -d ".checkout" ]
	then
		echo ".checkout dir does not exit"
		exit 1
	fi
	cat "deps.txt" | while read line
	do
		a=( $line )
		dir=${a[1]}
		sd=${a[2]}
		cloneto=".checkout/$dir"
		subdir="$cloneto/$sd"
		set -x
		depsdir="deps/$dir"
		mkdir -p $depsdir
		rsync -vcr --delete --exclude ".git" $subdir $depsdir
	done
fi
