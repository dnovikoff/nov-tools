if(NOV_INCLUDE_test)
    return()
endif()

set(NOV_INCLUDE_test 1)

FIND_PACKAGE( Boost 1.49 COMPONENTS unit_test_framework REQUIRED )

include(nov_tools)

IF(NOT NOV_PROXY_COMPILER)
	SET(NOV_PROXY_COMPILER "nov_cxx_proxy")
ENDIF()

SET(CMAKE_CXX_COMPILE_OBJECT "${NOV_PROXY_COMPILER} ${CMAKE_CXX_COMPILE_OBJECT}")

MACRO(NOV_BUILD_TEST test file)
	SET(extra_libs ${ARGV})
	NOV_LIST_POP(extra_libs 2)

	ADD_EXECUTABLE(${test} ${file})
	TARGET_LINK_LIBRARIES( ${test} ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY} )

	IF(extra_libs)
		TARGET_LINK_LIBRARIES(${test} ${extra_libs} )
	ENDIF()

	SET(passed "${test}.passed")

	ADD_CUSTOM_COMMAND(
		OUTPUT ${passed}
		COMMAND ${CMAKE_CURRENT_BINARY_DIR}/${test} && touch ${passed}
		DEPENDS ${test}
	)

	ADD_CUSTOM_TARGET(${test}.run
		COMMAND ${CMAKE_CURRENT_BINARY_DIR}/${test}
		DEPENDS ${test}
	)	
ENDMACRO()

MACRO(NOV_TESTSET case_name)
	SET(ALLTESTS)
	SET(ALLTESTS_RUN)
	SET(extra_libs ${ARGV})

	NOV_LIST_POP(extra_libs)

	FILE(GLOB files "*.cpp")
	
	IF(NOT files)
		MESSAGE(FATAL "No files to create unit testset")
	ENDIF()

	FOREACH(file ${files})
		GET_FILENAME_COMPONENT(name ${file} NAME_WE)
		SET(test ${name}_test)
		
		NOV_BUILD_TEST(${test} ${file} ${extra_libs})

		NOV_LIST_PUSH(ALLTESTS "${test}.passed")
		NOV_LIST_PUSH(ALLTESTS_RUN "${test}.run")
	ENDFOREACH()
	
	IF(ALLTESTS)
		ADD_CUSTOM_TARGET(${case_name} ALL DEPENDS ${ALLTESTS})
		# manual target
		ADD_CUSTOM_TARGET(${case_name}.run DEPENDS ${ALLTESTS_RUN})
	ENDIF()
ENDMACRO()

MACRO(NOV_COMPILE_TESTSET case_name)
	FILE(GLOB files "*.cpp")

	IF(files)
		FILE(GLOB fail_files "*.fail.cpp")
		IF(fail_files)
			SET_SOURCE_FILES_PROPERTIES(${fail_files} COMPILE_FLAGS -DNOV_FAIL_COMPILATION)
		ENDIF() 

		ADD_LIBRARY(${case_name} OBJECT ${files})
	ELSE()
		MESSAGE(FATAL "No files to create compilation testset")
	ENDIF()
ENDMACRO()
