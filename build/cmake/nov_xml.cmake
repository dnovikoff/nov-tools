if(NOV_INCLUDE_xml)
    return()
endif()

set(NOV_INCLUDE_xml 1)

include(nov_tools)

# TODO: find tool xmllint

# Validate xml with xsd file given, using xmllint as tool
# Will process xml file to target

MACRO(NOV_VALIDATE_XML_TARGET name xml xsd)
	SET(VALIDATE_TARGET ${name})

	NOV_FILE(XSD_FILE ${xsd})
	NOV_FILE(XML_FILE ${xml})
	
	ADD_CUSTOM_COMMAND(
		OUTPUT ${VALIDATE_TARGET}
		DEPENDS ${xml} ${xsd}

		COMMAND xmllint
		ARGS --schema ${XSD_FILE} ${XML_FILE} > ${VALIDATE_TARGET}

		COMMENT "Validating ${xml} with ${xsd}"
	)
ENDMACRO()

# +Automaticaly name target
MACRO(NOV_VALIDATE_XML xml xsd)
	NOV_VALIDATE_XML_TARGET(${xml}.validate ${xml} ${xsd})
	ADD_CUSTOM_TARGET(${xml}.run-validate ALL DEPENDS ${xml}.validate)
ENDMACRO()

