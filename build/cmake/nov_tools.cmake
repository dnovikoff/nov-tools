if(NOV_INCLUDE_tools)
    return()
endif()

set(NOV_INCLUDE_tools 1)

# Build both static and shared library 

# Return relative or absolute path
MACRO(NOV_FILE varname path)
	IF(${path} MATCHES "^/")
		SET(${varname} ${path})
	ELSE()
		SET(${varname} ${CMAKE_CURRENT_SOURCE_DIR}/${path})
	ENDIF()
ENDMACRO()

MACRO(NOV_SHARED_LIB libname)
	SET(sources ${ARGV})
	NOV_LIST_POP(sources)

	ADD_LIBRARY(${libname} SHARED ${sources})

	IF(NOT NOV_VERSION_SO)
		MESSAGE(FATAL_ERROR "NOV_VERSION_SO not set")
	ENDIF()
	
	SET_TARGET_PROPERTIES(${libname}
		PROPERTIES
			VERSION ${NOV_VERSION_SO}
	)

	INSTALL(TARGETS ${libname}
		LIBRARY DESTINATION lib COMPONENT lib
	)
ENDMACRO()

MACRO(ADD_BOTH_LIBS libname)
	SET(sources ${ARGV})
	NOV_LIST_POP(sources)

	SET(SHLIB ${libname}_shared)
	SET(STLIB ${libname}_static)

	ADD_LIBRARY(${STLIB} STATIC ${sources})
	ADD_LIBRARY(${SHLIB} SHARED ${sources})

	SET_TARGET_PROPERTIES(${STLIB} PROPERTIES OUTPUT_NAME ${libname})

	IF(NOT NOV_VERSION_SO)
		MESSAGE(FATAL_ERROR "NOV_VERSION_SO not set")
	ENDIF()
	
	SET_TARGET_PROPERTIES(${SHLIB} PROPERTIES
		OUTPUT_NAME ${libname}
		VERSION ${NOV_VERSION_SO}
	)

	INSTALL(TARGETS ${STLIB} ${SHLIB}
		LIBRARY DESTINATION lib
		ARCHIVE DESTINATION lib
	)
ENDMACRO()

MACRO(NOV_INSTALL_BIN_LINK target dir dep)
	SET(installed_file ../${dir}/${dep})
	SET(targetfile ${CMAKE_CURRENT_BINARY_DIR}/${target})

	ADD_CUSTOM_COMMAND(
		OUTPUT ${targetfile}.check
		# symblink could point to nowhere. Will lead to "no file exists" and recompile
		COMMAND ln -sf ${installed_file} ${targetfile}
		COMMAND touch ${targetfile}.check
		COMMENT "Creating symblink /usr/bin/${target}"
		DEPENDS ${dep}
	)
	ADD_CUSTOM_TARGET(${target}.run ALL DEPENDS ${targetfile}.check)
	INSTALL(PROGRAMS ${targetfile} DESTINATION bin)
ENDMACRO()

MACRO(NOV_APPEND varname value)
	IF(${varname})
		SET(${varname} "${${varname}}${value}")
	ELSE()
		SET(${varname} "${value}")
	ENDIF()
ENDMACRO()

MACRO(NOV_PREPEND varname value)
	IF(${varname})
		SET(${varname} "${value}${${varname}}")
	ELSE()
		SET(${varname} "${value}")
	ENDIF()
ENDMACRO()

SET(NOV_755_PERMISSIONS
	PERMISSIONS
		OWNER_EXECUTE OWNER_WRITE OWNER_READ
		GROUP_READ GROUP_EXECUTE
		WORLD_READ WORLD_EXECUTE
)

SET(NOV_DIR_PERMISSIONS
	DIRECTORY_${NOV_755_PERMISSIONS}
)

SET(NOV_DATA_PERMISSIONS
	PERMISSIONS
		OWNER_WRITE OWNER_READ
		GROUP_READ
		WORLD_READ
)

SET(NOV_EXE_PERMISSIONS
	${NOV_755_PERMISSIONS}
)

MACRO(NOV_INSTALL_PROG file rename comp)
	INSTALL(PROGRAMS ${file}
		${NOV_EXE_PERMISSIONS}
		DESTINATION bin
		RENAME ${rename}
		COMPONENT ${comp}
	)
ENDMACRO()

MACRO(NOV_FILES files dir)
	FILE(GLOB files "${CMAKE_CURRENT_SOURCE_DIR}/${dir}/*")
ENDMACRO()

MACRO(NOV_LIST_POP lst)
	SET(cnt ${ARGV1})
	IF(NOT cnt)
		SET(cnt 1)
	ENDIF()

	FOREACH(tmp RANGE 1 ${cnt})
		LIST(REMOVE_AT ${lst} 0)
	ENDFOREACH() 
ENDMACRO()

MACRO(NOV_LIST_PUSH lst val)
	SET(${lst} ${${lst}} ${val})  
ENDMACRO()

MACRO(NOV_TXT_REPLACE target file)
	SET(lst ${ARGV})
	NOV_LIST_POP(lst 2)

	SET(sedargs)

	FOREACH(item ${lst})
		SET(sedargs ${sedargs} -e "'s/${item}/g'")
	ENDFOREACH()

	#TODO: findbin sed
	ADD_CUSTOM_COMMAND(
		OUTPUT ${target}
		DEPENDS ${file}

		COMMAND "/bin/sed"
		ARGS ${sedargs} ${file} > ${target}
		COMMENT "Generating ${target} based on ${file}"
	)

	ADD_CUSTOM_TARGET(${target}.run ALL DEPENDS ${target})
ENDMACRO()

MACRO(NOV_GZIP target file)
	#TODO: findbin gz
	ADD_CUSTOM_COMMAND(
		OUTPUT ${target}
		DEPENDS ${file}

		COMMAND gzip
		ARGS -9 -c ${file} > ${target}
		COMMENT "Compressing into ${target} from ${file}"
	)

	ADD_CUSTOM_TARGET(${target}.run ALL DEPENDS ${target})
ENDMACRO()

MACRO(NOV_STRING_JOIN name sep atLeastOne)
	SET(TMP)
	SET(LST ${ARGV})
	NOV_LIST_POP(LST 2)

	FOREACH(item ${LST})
		IF(TMP)
			NOV_APPEND(TMP "${sep}")
		ENDIF()
		NOV_APPEND(TMP "${item}")
	ENDFOREACH()
	SET(${name} ${TMP})
ENDMACRO()
