if(NOV_INCLUDE_xsl)
    return()
endif()

set(NOV_INCLUDE_xsl 1)
# TODO: find tool saxonb-xslt

include(nov_tools)
include(nov_xml)

# Produce document, using xslt
MACRO(NOV_XSLT_ mode name xml xsl)
	SET(xsd ${ARGV4})

	SET(before_run)
	SET(after_run)

	IF(${mode} STREQUAL "DIR")
		SET(tmp ${name}.tmp)
		SET(CMD_TAIL outputDir="${tmp}" > ${name}.out)
		SET(before_run
			COMMAND rm
			ARGS -rf ${tmp}
		)
		SET(after_run
			COMMAND rsync
			# verbose, checksum-based, recursive, delete not existing
			ARGS -vcr --delete ${tmp}/ ${name}
			COMMAND touch
			ARGS ${name}
		)
	ELSEIF(${mode} STREQUAL "FILE")
		SET(CMD_TAIL > ${name})
	ELSE()
		MESSAGE(FATAL_ERROR "Unknown mode ${mode}")
	ENDIF()

	# user is allowed not to provide xsd file
	SET(XSL_DEPENDS)
	IF(xsd)
		NOV_VALIDATE_XML_TARGET(${name}.xsl-validate ${xml} ${xsd})
		SET(XSL_DEPENDS DEPENDS ${name}.xsl-validate ${xsl} ${NOV_XSL_EXTRA_DEPENDS})
	ELSE()
		SET(XSL_DEPENDS DEPENDS ${xml} ${xsl} ${NOV_XSL_EXTRA_DEPENDS})
	ENDIF()
	# clear depends
	SET(NOV_XSL_EXTRA_DEPENDS)

	NOV_FILE(XSL_FILE ${xsl})
	NOV_FILE(XML_FILE ${xml})

	ADD_CUSTOM_COMMAND(
		OUTPUT ${name}
		${XSL_DEPENDS}

		${before_run}
		COMMAND saxonb-xslt
		ARGS -ext:on -s:${XML_FILE} -xsl:${XSL_FILE} ${CMD_TAIL}
		${after_run}

		COMMENT "Saxon XSLT ${mode} ${xml} ${xsl}"
	)

	ADD_CUSTOM_TARGET("${name}.run" ALL DEPENDS ${name})
ENDMACRO()

MACRO(NOV_XSLT)
	NOV_XSLT_(FILE ${ARGV})
ENDMACRO()

MACRO(NOV_XSLT_DIR)
	NOV_XSLT_(DIR ${ARGV})
ENDMACRO()
